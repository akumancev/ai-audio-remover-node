# AI Audio remover (node)

## How to start

1. Create .env file:

```bash
cp .env.example .env
```

2. Install dependencies:

```bash
npm install
```

3. Run server:

```bash
npm run start
```
