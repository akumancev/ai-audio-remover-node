const fs = require("node:fs");
const path = require("node:path");
const express = require("express");
const axios = require("axios");
const multer = require("multer");
const FormData = require("form-data");
require('dotenv').config();

const app = express();
const port = 3000;

const pythonServerURL = process.env.PYTHON_SERVER_URL;
const authKey = process.env.AUTH_KEY;

app.use(express.json());

const storage = multer.memoryStorage();
const upload = multer({ storage: storage });

app.post("/upload", upload.single("file"), async (req, res) => {
  try {
    if (!req.file) {
      return res.status(400).json({ error: "No file uploaded" });
    }

    const audioFileBuffer = req.file.buffer;

    const tempDir = path.join(__dirname, "temp");
    if (!fs.existsSync(tempDir)) {
      fs.mkdirSync(tempDir);
    }

    const uniqueFileName = Date.now() + "_" + req.file.originalname;
    const tempFilePath = path.join(tempDir, uniqueFileName);

    fs.writeFileSync(tempFilePath, audioFileBuffer);

    const form = new FormData();
    form.append("file", fs.createReadStream(tempFilePath));

    const requestData = {
      tta: true,
      postprocess: false,
    };
    form.append("req_data", JSON.stringify(requestData));

    const pythonResponse = await axios.post(
      `${pythonServerURL}/separate/`,
      form,
      {
        headers: {
          ...form.getHeaders(),
          "auth-key": authKey,
        },
        responseType: "stream",
      }
    );

    res.set(pythonResponse.headers);
    pythonResponse.data.pipe(res);

    // Cleanup temporary file
    pythonResponse.data.on("end", () => {
      fs.unlink(tempFilePath, (err) => {
        if (err) {
          console.error("Error deleting the temporary file:", err);
        }
      });
    });
  } catch (error) {
    console.error(error);
    res
      .status(500)
      .json({ error: "An error occurred while processing the audio file" });
  }
});

app.listen(port, () => {
  console.log(`Node.js server is running on port ${port}`);
});
